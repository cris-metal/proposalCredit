package br.com.calcard.repository;

import br.com.calcard.domain.Customer;
import br.com.calcard.web.dtos.CustomerDto;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Customer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findByCpf(String cpf);
}
