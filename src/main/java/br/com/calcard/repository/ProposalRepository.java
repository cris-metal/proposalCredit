package br.com.calcard.repository;

import br.com.calcard.domain.Proposal;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Proposal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProposalRepository extends JpaRepository<Proposal, Long> {
    Proposal findByCustomerId(Long customerId);
}
