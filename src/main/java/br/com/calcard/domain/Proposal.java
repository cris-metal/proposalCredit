package br.com.calcard.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Proposal.
 */
@Entity
@Table(name = "proposal")
public class Proposal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "evaluation", nullable = false)
    private String evaluation;

    @NotNull
    @Column(name = "situation", nullable = false)
    private Boolean situation;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Customer customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public Proposal evaluation(String evaluation) {
        this.evaluation = evaluation;
        return this;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public Proposal situation(Boolean situation) {
        this.situation = situation;
        return this;
    }

    public Boolean isSituation() {
        return situation;
    }

    public void setSituation(Boolean situation) {
        this.situation = situation;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Proposal proposal = (Proposal) o;
        if (proposal.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), proposal.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Proposal{" +
            "id=" + getId() +
            ", evaluation='" + getEvaluation() + "'" +
            ", situation='" + isSituation() + "'" +
            "}";
    }
}
