package br.com.calcard.domain.enumeration;

/**
 * The MaritalStatus enumeration.
 */
public enum MaritalStatus {
    Solteiro, Casado, Viuvo, Divorciado
}
