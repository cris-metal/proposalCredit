package br.com.calcard.domain.enumeration;

/**
 * The GenderEnum enumeration.
 */
public enum GenderEnum {
    Feminino, Masculino
}
