package br.com.calcard.domain.enumeration;

/**
 * The StateEnum enumeration.
 */
public enum StateEnum {
    AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO

    }
