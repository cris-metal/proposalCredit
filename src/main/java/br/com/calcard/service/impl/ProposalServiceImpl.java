package br.com.calcard.service.impl;

import br.com.calcard.domain.Customer;
import br.com.calcard.domain.enumeration.MaritalStatus;
import br.com.calcard.service.ProposalService;
import br.com.calcard.domain.Proposal;
import br.com.calcard.repository.ProposalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static br.com.calcard.domain.enumeration.MaritalStatus.Divorciado;
import static br.com.calcard.domain.enumeration.MaritalStatus.Viuvo;


/**
 * Service Implementation for managing Proposal.
 */
@Service
@Transactional
public class ProposalServiceImpl implements ProposalService{
    private final String disapprovate = "reprovado pela política de crédito";
    private final String lowIncome = "renda baixa";

    private final Logger log = LoggerFactory.getLogger(ProposalServiceImpl.class);

    private final ProposalRepository proposalRepository;
    public ProposalServiceImpl(ProposalRepository proposalRepository) {
        this.proposalRepository = proposalRepository;
    }

    /**
     * Save a proposal.
     *
     * @param proposal the entity to save
     * @return the persisted entity
     */
    @Override
        public Proposal save(Proposal proposal, Customer customer) {
        log.debug("Request to save Proposal : {}", proposal);

        proposal.setCustomer(customer);
        String evaluation = this.processProposal(customer.getAge(), customer.getMaritalStatus(), customer.getDependents(), customer.getIncome());
        proposal.setEvaluation(evaluation);

        if (evaluation.equals(disapprovate) || evaluation.equals(lowIncome))
            proposal.setSituation(false);
        else
            proposal.setSituation(true);

        return proposalRepository.save(proposal);
    }

    /**
     *  Get all the proposals.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Proposal> findAll(Pageable pageable) {
        log.debug("Request to get all Proposals");
        return proposalRepository.findAll(pageable);
    }

    /**
     *  Get one proposal by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Proposal findOne(Long id) {
        log.debug("Request to get Proposal : {}", id);
        return proposalRepository.findOne(id);
    }

    @Override
    public Proposal findByCustomerId(Long customerId) {
        log.debug("Request to get Proposal by customer: {}", customerId);
        return proposalRepository.findByCustomerId(customerId);
    }


    @Override
    public String processProposal(Integer age, MaritalStatus maritalStatus, Integer dependents, Double income) {
        if (isDisapprovedPolicy(age, maritalStatus, dependents, income))
            return this.disapprovate;

        else if (isLowIncome(income))
            return this.lowIncome;

        else{
            return String.valueOf(calcLimit(age, maritalStatus, dependents, income));
        }

    }

    /**
     * Se Cliente divorciado com dependentes e renda inferior a 5000,00 reprovado por politica de credito
     * Se Cliente viuvo com dependentes e renda inferiror a 3000,00 reprovado por politica de credito
     * Se menor de 16 anos reprovado por politica de credito
     * @param age
     * @param maritalStatus
     * @param dependents
     * @param income
     * @return
     */
    private boolean isDisapprovedPolicy(Integer age, MaritalStatus maritalStatus, Integer dependents, Double income){
        if (age < 16)
            return true;

        else if (maritalStatus.equals(Divorciado) && dependents > 0 && income < 5000)
            return true;

        else if (maritalStatus.equals(Viuvo) && dependents > 0 && income < 3000)
            return true;

        return false;
    }

    /**
     * Verifica renda abaixo de 1000,00
     * @param income
     * @return
     */
    private boolean isLowIncome(Double income){
        if (income < 1000)
            return true;

        return false;
    }

    /**
     * Avalia o cadastro e em caso de aprovacao, calculo o limite liberado
     * @param age
     * @param maritalStatus
     * @param dependents
     * @param income
     * @return
     */
    private Double calcLimit(Integer age, MaritalStatus maritalStatus, Integer dependents, Double income){
        int points = pointsByAge(age) + pointsByDependents(dependents) + pointsByMaritalStatus(maritalStatus) + pointsByIncome(income, maritalStatus, dependents) ;

        return income * ((double)points/100);
    }

    /**
     * Calcula os pontos baseados na idade do cliente:
     * 16 - 22 = 6
     * 23 - 28 = 7
     * 29 - 35 = 8
     * 36 - 45 = 9
     * 46 - 55 = 8
     * 56 - 65 = 7
     * >= 66   = 6
     * @param age
     * @return
     */
    private Integer pointsByAge(int age){
        if (age >= 16 && age <= 22)
            return 6;

        else if (age >= 23 && age <= 28)
            return 7;

        else if (age >= 29 && age <= 35)
            return 8;

        else if (age >= 36 && age <= 45)
            return 9;

        else if (age >= 46 && age <= 55)
            return 8;

        else if (age >= 56 && age <= 65)
            return 7;

        else if (age >= 66)
            return 6;

        return 0;
    }

    /**
     * Retorna os pontos por estado civil
     * @param maritalState
     * Casado = 8
     * Solteiro = 6
     * Viuvo = 3
     * Divorciado = 0
     * @return
     */
    private int pointsByMaritalStatus(MaritalStatus maritalState){

        switch (maritalState) {
            case Casado:
                return 8;

            case Solteiro:
                return 6;

            case Viuvo:
                return 3;

            case Divorciado:
                return 0;

            default:
                return 0;

        }
    }

    /**
     * Retorna os pontos por numero de dependentes:
     * 0 dependentes = 10
     * 1 dependente = 7
     * 2 dependentes = 5
     * 3 dependentes = 3
     * 4 dependentes = 1
     * mais de 3 dependentes = 0
     * @param dependents
     * @return
     */
    private int pointsByDependents(int dependents){
        switch (dependents){
            case 0:
                return 10;

            case 1:
                return 7;

            case 2:
                return 5;

            case 3:
                return 3;

            case 4:
                return 1;

            default:
                return 0;

        }
    }

    /**
     * Retorna a pontuacao com base na renda:
     * >= 4000,00 = 5
     * 3000,00 - 3999,99 = 3
     * Obs: Se o cliente for divorciado ou viuvo e/ou ter mais de 3 filhos, não recebe a pontuação
     * @param income
     * @param maritalStatus
     * @param dependents
     * @return
     */
    private int pointsByIncome(Double income, MaritalStatus maritalStatus, int dependents){
        if (isValidIncomePoints(maritalStatus, dependents))
            if (income >= 4000)
                return 5;
            else if (income >= 3000){
                return 3;
            }

        return 0;
    }

    /**
     * Se o cliente for divorciado ou viuvo e/ou ter mais de 3 filhos, não recebe a pontuação
     * por renda
     * @return
     */
    private boolean isValidIncomePoints(MaritalStatus maritalStatus, int dependents){
        if (maritalStatus.equals(Divorciado) || maritalStatus.equals(Viuvo))
            return false;
        else if (dependents > 3)
            return false;

        return true;
    }
}
