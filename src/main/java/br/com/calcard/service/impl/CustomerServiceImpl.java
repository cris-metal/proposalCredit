package br.com.calcard.service.impl;

import br.com.calcard.domain.Customer;
import br.com.calcard.domain.Proposal;
import br.com.calcard.repository.CustomerRepository;
import br.com.calcard.service.CustomerService;
import br.com.calcard.web.dtos.CustomerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Customer.
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private ProposalServiceImpl proposalService;

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * Save a customer.
     *
     * @param customer the entity to save
     * @return the persisted entity
     */
    @Override
    public CustomerDto save(Customer customer) {
        log.debug("Request to save Customer : {}", customer);
        Customer newCustomer = customerRepository.saveAndFlush(customer);
        newCustomer.setCpf(newCustomer.getCpf().replace(".","").replace("-",""));

        Proposal proposal = new Proposal();
        proposal.setCustomer(newCustomer);

        String evaluation = proposalService.processProposal(newCustomer.getAge(), newCustomer.getMaritalStatus(), newCustomer.getDependents(), newCustomer.getIncome());

        proposal.setEvaluation(evaluation);

        proposalService.save(proposal, newCustomer);

        CustomerDto customerDto = new CustomerDto(newCustomer, proposal);

        return customerDto;
    }

    /**
     *  Get all the customers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Customer> findAll(Pageable pageable) {
        log.debug("Request to get all Customers");
        return customerRepository.findAll(pageable);
    }

    /**
     *  Get one customer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Customer findOne(Long id) {
        log.debug("Request to get Customer : {}", id);
        return customerRepository.findOne(id);
    }

    @Override
    public Customer findByCpf(String cpf) {
        log.debug("Request to get Customer by cpf: {}", cpf);
        return customerRepository.findByCpf(cpf);
    }
}
