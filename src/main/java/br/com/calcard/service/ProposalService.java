package br.com.calcard.service;

import br.com.calcard.domain.Customer;
import br.com.calcard.domain.Proposal;
import br.com.calcard.domain.enumeration.MaritalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Proposal.
 */
public interface ProposalService {

    /**
     * Save a proposal.
     *
     * @param proposal the entity to save
     * @return the persisted entity
     */
    Proposal save(Proposal proposal, Customer customer);

    /**
     *  Get all the proposals.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Proposal> findAll(Pageable pageable);

    /**
     *  Get the "id" proposal.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Proposal findOne(Long id);

    Proposal findByCustomerId(Long customerId);


    String processProposal(Integer age, MaritalStatus maritalStatus, Integer dependents, Double income);
}
