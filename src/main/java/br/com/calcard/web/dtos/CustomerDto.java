package br.com.calcard.web.dtos;

import br.com.calcard.domain.Customer;
import br.com.calcard.domain.Proposal;
import br.com.calcard.domain.enumeration.GenderEnum;
import br.com.calcard.domain.enumeration.MaritalStatus;
import br.com.calcard.domain.enumeration.StateEnum;

/**
 * Created by cris on 24/10/2017.
 */
public class CustomerDto {
    private Long id;
    private String name;
    private String cpf;
    private Integer age;
    private GenderEnum gender;
    private MaritalStatus maritalStatus;
    private StateEnum state;
    private Integer dependents;
    private Double income;
    private String evaluation;
    private Boolean situation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        this.state = state;
    }

    public Integer getDependents() {
        return dependents;
    }

    public void setDependents(Integer dependents) {
        this.dependents = dependents;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public Boolean getSituation() {
        return situation;
    }

    public void setSituation(Boolean situation) {
        this.situation = situation;
    }

    public CustomerDto() {
        super();
    }

    public CustomerDto(Customer customer, Proposal proposal) {
        this(customer.getId(), customer.getName(), customer.getCpf(), customer.getAge(), customer.getGender(),
            customer.getMaritalStatus(), customer.getState(), customer.getDependents(), customer.getIncome(),
            proposal.getEvaluation(), proposal.isSituation());
    }

    public CustomerDto(Long id, String name, String cpf, Integer age, GenderEnum gender,
                       MaritalStatus maritalStatus, StateEnum state, Integer dependents, Double income,
                       String evaluation, Boolean situation) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.age = age;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.state = state;
        this.dependents = dependents;
        this.income = income;
        this.evaluation = evaluation;
        this.situation = situation;
    }
}
