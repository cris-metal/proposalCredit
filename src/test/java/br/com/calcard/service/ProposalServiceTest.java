package br.com.calcard.service;

import br.com.calcard.domain.enumeration.MaritalStatus;
import br.com.calcard.service.impl.ProposalServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cris on 21/10/2017.
 */
public class ProposalServiceTest {
    private ProposalService proposalService;

    @Before
    public void setUp() throws Exception {
        proposalService = new ProposalServiceImpl(null);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void processProposal() throws Exception {
        Double proposal1 = Double.parseDouble(proposalService.processProposal(28, MaritalStatus.Solteiro,0, Double.valueOf(2500)));
        assertTrue( proposal1 >= 500 || proposal1 <= 1000);

        Double proposal2 = Double.parseDouble(proposalService.processProposal(17, MaritalStatus.Solteiro,0, Double.valueOf(1000)));
        assertTrue(proposal2 >= 100 || proposal2 <= 500);

        assertEquals(proposalService.processProposal(56, MaritalStatus.Divorciado,2, Double.valueOf(2000)), "reprovado pela política de crédito");

        Double proposal3 = Double.parseDouble(proposalService.processProposal(68, MaritalStatus.Casado,3, Double.valueOf(8000)));
        assertTrue(proposal3 >= 1500 || proposal3 <= 2000);

        Double proposal4 = Double.parseDouble(proposalService.processProposal(61, MaritalStatus.Casado,3, Double.valueOf(5000)));
        assertTrue(proposal4 >= 1000 || proposal4 <= 1500);

        assertEquals(proposalService.processProposal(56, MaritalStatus.Divorciado,2, Double.valueOf(2000)), "reprovado pela política de crédito");

        assertEquals(proposalService.processProposal(45, MaritalStatus.Divorciado,1, Double.valueOf(2000)), "reprovado pela política de crédito");

        Double proposal5 = Double.parseDouble(proposalService.processProposal(30, MaritalStatus.Casado,2, Double.valueOf(8000)));
        assertTrue(proposal5 > 2000);

        Double proposal6 = Double.parseDouble(proposalService.processProposal(33, MaritalStatus.Casado,1, Double.valueOf(10000)));
        assertTrue(proposal6 > 2000);

        assertEquals(proposalService.processProposal(19, MaritalStatus.Solteiro, 1, Double.valueOf(400)), "renda baixa");

        assertEquals(proposalService.processProposal(63, MaritalStatus.Viuvo,3, Double.valueOf(1500)), "reprovado pela política de crédito");

        Double proposal7 = Double.parseDouble(proposalService.processProposal(28, MaritalStatus.Solteiro,2, Double.valueOf(2500)));
        assertTrue(proposal7 >= 100 || proposal7 <= 500);

        assertEquals(proposalService.processProposal(16, MaritalStatus.Solteiro, 0, Double.valueOf(500)), "renda baixa");

        Double proposal8 = Double.parseDouble(proposalService.processProposal(30, MaritalStatus.Casado,5, Double.valueOf(8000)));
        assertTrue(proposal8 >= 1000 || proposal8 <= 1500);

        Double proposal9 = Double.parseDouble(proposalService.processProposal(33, MaritalStatus.Viuvo,0, Double.valueOf(10000)));
        assertTrue(proposal9 >= 1000 || proposal9 <= 1500);
    }

}
